/* TOUKLAKOS RECORDS 2020 */
/* GARTNER ESTHER & BRIERE BENJAMIN */

/* GESTION DE L'APPARITION AU SCROLL */
ScrollReveal({ distance: '400px' });
ScrollReveal().reveal('.event-gauche', { origin: 'left' });
ScrollReveal().reveal('.event-droite', { origin: 'right' });
ScrollReveal({ distance: '0px' });
ScrollReveal().reveal('.artiste', { easing: 'ease-in' });
/* FIN */

/* FONCTION AFFICHAGE ALBUM */
async function test(artist){
  document.getElementById('tracks').innerHTML=""; // si la div d'ID tracks contient déja des éléments elle est vidée
  string = 'https://itunes.apple.com/search?term='+artist+'&limit=25'; // création de la requete pour l'api d'itunes
  const response = await fetch(string); // appel de la requete itunes
  const myJson = await response.json(); // reponse convertit en json
  //console.log(response, myJson);

  myJson.results.forEach(async function(e){ // traitement du json
    var tracks = document.getElementById('tracks');
    var name = e.trackName; // du json on récupère le nom du titre
    var albumName = e.collectionCensoredName // du json on récupère le nom de l'album
    var audioURL = e.previewUrl // du json on récupère l'adresse de l'apercu audio du titre
    var album = document.createElement("div"); // création d'une div vide qui contiendra l'ensemble des informations du morceau
    var albumCover = document.createElement("div"); // création d'une div vide qui contiendra la cover d'album
    var albumTitle = document.createElement("div"); // création d'une div vide qui contiendra le titre du morceau
    var audio = document.createElement("audio"); // création d'une balise audio vide qui contiendra l'apercu du titre'
    audio.setAttribute("controls",""); // dans balise audio on ajoute le parametre controls nécessaire à la balise audio
    audio.setAttribute("src",audioURL); // dans balise audio on ajoute l'url de l'apercu

    var img = e.artworkUrl100; // du json on récupère la cover d'album en 100 * 100
    if (e.artworkUrl100){ // si la cover de l'album existe on va chercher à en trouver une d'une meilleur qualité sur une autre API
      var chaine = 'http://ws.audioscrobbler.com/2.0/?method=album.search&album='+albumName+'&api_key=c6b347a230844e6d79e722ea6679833c&format=json'; // création de la requete pour l'api d'audioscrobbler
      //console.log(chaine)
      const response1 = await fetch('http://ws.audioscrobbler.com/2.0/?method=album.search&album='+albumName+'&api_key=c6b347a230844e6d79e722ea6679833c&format=json'); // appel de la requete audioscrobbler
      const myJson1 = await response1.json();
      var artwork = myJson1.results.albummatches.album[0].image[3]['#text'];; // récupération de l'url de l'image en meilleur résolution
      if (artwork == ""){ // si l'image n'existe pas dans ce cas on la remplace par une cover générique du label
        artwork = "images/touklakos2.png";
      }
      albumCover.style.background = `no-repeat url(${artwork}) center/100%` // création d'un style à la div albumCover qui contient la cover d'album en background
    }

    /* REMPLISSAGE de la page html avec les titres trouvés */
    album.classList.add("album");
    tracks.appendChild(album);

    albumCover.classList.add("albumCover");
    album.appendChild(albumCover);

    albumTitle.classList.add("albumTitle");
    album.appendChild(albumTitle);
    albumTitle.innerHTML = name;

    audio.classList.add("tracksPreview");
    album.appendChild(audio);
    /* FIN */
  });
};
/* FIN */

/* RECUPERATION VALEUR DU FORMULAIRE */
document.getElementById("buttonSearch").onclick = function(){
  var a = document.getElementById('artistSearch');
  var artist = a.value;
  console.log(artist);
  test(artist);
};
/* FIN */

/* MODE NUIT*/
function nuit()
{
  document.body.classList.toggle("nuit");
};
/* FIN */
